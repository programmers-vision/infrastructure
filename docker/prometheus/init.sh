#!/usr/bin/env bash
export PATH=/prometheus:$PATH

envsubst < /etc/prometheus/prometheus.yml > /etc/prometheus/prometheus.yml.tmp
mv /etc/prometheus/prometheus.yml.tmp /etc/prometheus/prometheus.yml
/prometheus/prometheus --config.file=/etc/prometheus/prometheus.yml
