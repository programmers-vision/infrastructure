# Contributing to the Vision Infrastructure
We love and welcome community contributions, so below you will find the
process on how to get your suggestion from a written issue into
production code!

## Topics
- [Contributing to the Vision Infrastructure](#contributing-to-the-vision-infrastructure)
  - [Topics](#topics)
  - [Reporting Issues](#reporting-issues)
  - [Working on Issues](#working-on-issues)
  - [Contributing to the Programmers Palace Infrastructure](#contributing-to-the-programmers-palace-infrastructure)
    - [Prepare your environment](#prepare-your-environment)
    - [Development and Testing](#development-and-testing)

## Reporting Issues
Before reporting an issue, check our backlog of open issues to see if someone 
else has already reported it. If so, feel free to add your scenario, or 
additional information, to the discussion. Or simply "subscribe" to it by
enabling the "Notifications" toggle switch to be notified when progress is 
made.

If you find a new issue with the project we'd love to hear about it! The most 
important aspect of a bug report is that it includes enough information for us 
to reproduce it. So, please include as much detail as possible and try to 
remove the extra stuff that doesn't really relate to the issue itself. The 
easier it is for us to reproduce it, the faster it'll be fixed!

Please don't include any private/sensitive information in your issue!

## Working on Issues
Once you have decided to contribute to the Vision Infrastructure by working 
on an issue, check our backlog of open issues looking for any that do not 
have a "development" label attached to it. Often issues will be assigned 
to someone, to be worked on at a later time. If you have the time to work 
on the issue now add yourself as an assignee, and set the "development" 
label if you’re a member of the "Programmers Vision" team. If you can not set 
the label, just add a quick comment in the issue asking for the "development"
label to be set and a member will do so for you.

## Contributing to the Programmers Palace Infrastructure
This section describes how to start a contribution to the Programmers 
Palace Infrastructure.

### Prepare your environment
Read the installation instructions in the [README](./README.md#installation).

### Development and Testing
We highly recommend and encourage you to work on feature branches. In
particular `feat/your-feature`. After having setup your feature branch 
and having applied all necessary changes, you can submit a Pull Request 
to our repository. We will review and merge if we deem it a good addition
or bug-fix to the infrastructure!  
Every Pull Request matters be it a one-line change that has impact on the
way our infrastructure deploys, a type or a huge bulk of infrastructure 
code that suggests us to use a new service, we will check them all in a
timely manner.