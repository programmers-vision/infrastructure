resource "docker_image" "postgres" {
  name = "${var.company_name}/postgres"
  build {
    path = "docker/postgres"
    tag  = ["${var.company_name}/postgres:1.0"]
    label = {
      "author" = var.infrastructure_maintainer
    }
  }
}

resource "docker_container" "postgres" {
  image = docker_image.postgres.latest
  name  = "${var.company_prefix}-postgres"

  env = [
    # Postgres Admin
    "POSTGRES_USER=${var.database_user}",
    "POSTGRES_PASSWORD=${var.database_password}",

    # Postgres Bot DB
    "DISCORD_BOT_DB=${var.company_prefix}_bot_db",

    # Postgres Grafana Annotations
    "GRAFANA_USER=${var.company_prefix}_grafana",
    "GRAFANA_PASSWORD=${var.grafana_admin_password}",
    "GRAFANA_ANNOTATIONS_DB=${var.company_prefix}_grafana_db",
  ]

  volumes {
    volume_name    = docker_volume.postgres_data.name
    container_path = "/var/lib/postgresql/data"
  }

  networks_advanced {
    name = docker_network.internal.name
  }

  restart = "always"
}

resource "docker_volume" "postgres_data" {
  name = "${var.company_prefix}-postgres-data"
}
