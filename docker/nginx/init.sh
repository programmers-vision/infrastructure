#!/bin/bash

# Wait until Certificates are available
if [ $ENVIRONMENT = "prod" ]; then
  echo "Waiting for certificate generation..."
  while [ ! -d "/etc/letsencrypt/live" ]; do sleep 1; done
  echo "Detected Certificates..."
  echo "Enabling Prod-Only Routes"
  for file in /etc/nginx/template/*.disabled; do
    name=$(basename $file .disabled)
    echo "Enabling $name"
    mv $file /etc/nginx/template/$name
  done
fi


# Generate Configuration from Template Files
echo "Proceeding to set up nginx configuration..."
export DOLLAR='$'
for file in /etc/nginx/template/*.template; do
  name=$(basename $file .template)
  echo "Generating $name"
  envsubst < $file > /etc/nginx/conf.d/$name
done

# Start Up NGinx
echo "Starting nginx..."
/docker-entrypoint.sh nginx -g 'daemon off;'