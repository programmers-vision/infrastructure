#!/bin/bash
CERT_ROOT=/etc/letsencrypt/live/$DOMAIN

chmod 755 /etc/letsencrypt/{live,archive}
chmod 640 $CERT_ROOT/privkey.pem
